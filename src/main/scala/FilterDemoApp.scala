import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac045 on 2017/5/8.
  */
object FilterDemoApp extends App{
  val conf = new SparkConf().setAppName("FilterDemo")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  // odds
  //sc.parallelize(1 to 100).filter(i=>i%2==1).foreach(v=>println(v))
  //sc.textFile("./names.txt").foreach(println)


  //firstLetterIsH
  val char: Char='H'
  val str: String="H"
  sc.textFile("./names.txt")
  //.filter(name=>name.head=='H')
    .filter(name=>name.startsWith("H")).foreach(println)

}
