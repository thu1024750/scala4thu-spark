import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac045 on 2017/5/8.
  */
object WordCountApp extends App{
  val conf = new SparkConf().setAppName("WordCount")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val lines=sc.textFile("/Users/mac045/Downloads/spark-doc.txt")
  //lines.take(10).foreach(println)
  //lines.flatMap(str=>str.split(" ").toList).take(10).foreach(println)

  val words=lines.flatMap(i=>i.split(" "))
  //小考必考：但不可以用這種方法
  //words.groupBy(str=>str).mapValues(_.size).foreach(println)

  //words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr).foreach(println)
  words.map(_->1).reduceByKey(_+_).foreach(println)
  readLine()

}
