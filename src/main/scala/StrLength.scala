import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac045 on 2017/5/1.
  */
object StrLength extends App{
  val conf = new SparkConf().setAppName("StrLength")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val nums=sc.textFile("nums.txt")
  val strLength:RDD[Int]=nums.map(_.length)

//A
  //  println(strLength.collect().toList)
//B
  strLength.foreach(len=>println(len))

}
